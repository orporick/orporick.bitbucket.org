---
layout: page
title: Corsi
permalink: /corsi/
---

Per i miei corsi cerco di attenermi ai curricoli dipartimentali della
scuola:

 * [Fisica](http://win.liceomarinelli.org/pof/curricoli_13_14/fisica.pdf)
 * [Matematica](http://win.liceomarinelli.org/pof/curricoli_13_14/matematica.pdf)


