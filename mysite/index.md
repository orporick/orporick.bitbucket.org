---
layout: default
---

{::options parse_block_html="true" /}
<div class="home_image_container">
<div class="home_image">
![Photo of Me](https://lh3.googleusercontent.com/-K_82y82RWYE/VAWRNJhpKLI/AAAAAAAAOaw/HLGCEYWn9dM/s288/P_20140820_123932.jpg)
</div>
</div>

Benvenuti. 

Mi chiamo Riccardo Giannitrapani ed insegno Matematica e Fisica presso
il [Liceo Scientifico "G.Marinelli" di Udine](http://www.liceomarinelli.gov.it).

In questo piccolo sito raccolgo tutto il materiale e le informazioni
sui corsi che tengo presso la mia scuola, le attività didattiche ed in
generale tutto ciò che attiene al mio ruolo di insegnante. Raccolgo
qui anche un piccolo blog di idee ed appunti sfusi sull'insegnamento,
la matematica e la fisica.

Ogni commento è gradito (potete usare i riferimenti a fondo pagina per
contattarmi).

$$ e^{i\pi} + 1 = 0 $$






